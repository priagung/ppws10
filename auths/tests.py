from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from .apps import *
from .views import *
from django.contrib.auth.forms import User

class UnitTest(TestCase):
        def testAuthsApp(self):
                self.assertEqual(AuthsConfig.name, 'auths')
                self.assertEqual(apps.get_app_config(
                'auths').name, 'auths')

        def testRegisterURL(self):
                response = Client().get('/register/')
                self.assertEqual(response.status_code, 200)
                self.assertNotEqual(response.status_code, 404)
        
        def testLoginURL(self):
                response = Client().get('/login/')
                self.assertEqual(response.status_code, 200)
                self.assertNotEqual(response.status_code, 404)
        
        def testRegisterTemplate(self):
                response = Client().get('/register/')
                self.assertTemplateUsed(response, 'register.html')
        
        def testLoginTemplate(self):
                response = Client().get('/login/')
                self.assertTemplateUsed(response, 'registration/login.html')
                self.assertContains(response, 'login')
        
        def testRegisterFunction(self):
                found = reverse('auths:register')
                self.assertEqual(resolve(found).func, register)
                
        def testRegisterform(self):
                response = self.client.post(reverse('auths:register'), data={'username': 'testrandom1', 'password1': 'randomparah123nice', 'password2': 'randomparah123nice'})
                count = User.objects.all().count()
                input = User.objects.all().first()
                self.assertEqual(input.username, 'testrandom1')
                self.assertEqual(count, 1)
                self.assertNotEqual(count, 0)
