from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = "auths"
urlpatterns = [
    path('register/', views.register, name="register"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(next_page='auths:homepage'), name="logout"),
    path('', views.homepage, name="homepage")
]
